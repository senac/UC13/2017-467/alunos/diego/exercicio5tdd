package br.com.senac.test;

import br.com.senac.ex5TDD.ClassifacaNumero;
import org.junit.Test;
import static org.junit.Assert.*;

public class ClassificaNumeroTest {

    public ClassificaNumeroTest() {
    }

    @Test
    public void numero4SeraPar() {
        int numero = 4;
        boolean resultado = ClassifacaNumero.isPar(numero);
        assertTrue(resultado);
    }
    
    @Test
    public void numero6SeraPar() {
        int numero = 6;
        boolean resultado = ClassifacaNumero.isPar(numero);
        assertTrue(resultado);
    }
    
    @Test
    public void numero40SeraPar() {
        int numero = 40;
        boolean resultado = ClassifacaNumero.isPar(numero);
        assertTrue(resultado);
    }

    @Test
    public void numero9NaoSeraPar() {
        int numero = 9;
        boolean resultado = ClassifacaNumero.isPar(numero);
        assertFalse(resultado);
    }
    
    @Test
    public void numero35NaoSeraPar() {
        int numero = 35;
        boolean resultado = ClassifacaNumero.isPar(numero);
        assertFalse(resultado);
    }

    @Test
    public void numero12SeraPosivo() {
        int numero = 12;
        boolean resultado = ClassifacaNumero.isPositivo(numero);
        assertTrue(resultado);
    }
    
    @Test
    public void numero70SeraPosivo() {
        int numero = 70;
        boolean resultado = ClassifacaNumero.isPositivo(numero);
        assertTrue(resultado);
    }

}
